﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Sockets;
//using System.Text;
//using System.Threading.Tasks;

//namespace UDP_Server
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            int buf = 512;
//            bool done = false;
//            UdpClient listener = new UdpClient(11000);
//            IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, 11000);
//            string received_data;
//            byte[] receive_byte_array;
//            try

//            {


//                while (!done)
//                {
//                    Console.WriteLine("Waiting for broadcast");
//                    // this is the line of code that receives the broadcase message.

//                    // It calls the receive function from the object listener (class UdpClient)

//                    // It passes to listener the end point groupEP.

//                    // It puts the data from the broadcast message into the byte array

//                    // named received_byte_array.

//                    // I don't know why this uses the class UdpClient and IPEndPoint like this.

//                    // Contrast this with the talker code. It does not pass by reference.

//                    // Note that this is a synchronous or blocking call.

//                    receive_byte_array = listener.Receive(ref groupEP);
//                    Console.WriteLine("Received a broadcast from {0}", groupEP.ToString());
//                    received_data = Encoding.ASCII.GetString(receive_byte_array, 0, receive_byte_array.Length);
//                    Console.WriteLine("data follows \n{0}\n\n", received_data);
//                    listener.Connect(groupEP);
//                    listener.Send(receive_byte_array, 1024);
//                }
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e.ToString());
//            }
//            listener.Close();
//        }
//    }
//}

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UDP_Server
{
    public class Server
    {

        //Label Data
        public enum DataType
        {
            SYN = 0, SYNACK = 1, ACK = 2, FIN = 3, FINACK = 4, DATA = 5     //pengurutan data
        }


        
        struct Data
        {
            public int status;
            public int packet;
        }

        public static void Main(string[] args)
        {
            //Inisialisasi data

            byte[] data = new byte[1024];
            Data packet = new Data();
            DataType tipe = DataType.SYN;
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 11000);
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 11000);
            var newsock = new UdpClient(ipep);
            int counter = 0;
            bool stop = false;

            Console.WriteLine("Waiting for client...");

            while (stop == false)
            {
                byte[] send_buffer = new byte[1024];
                data = newsock.Receive(ref sender);

                string rcv = Encoding.ASCII.GetString(data);
                packet.status = Convert.ToInt16(rcv.Substring(0, rcv.IndexOf("-")));
                packet.packet = Convert.ToInt16(rcv.Substring(rcv.IndexOf("-") + 1));
                Console.WriteLine("Recv: stats {0} data {1}", packet.status, packet.packet);

                if (packet.status == (int)DataType.SYN)
                {
                    Console.WriteLine("Server got SYN!");

                    tipe = DataType.SYNACK;

                    //send synack to client
                    packet.packet = 0;
                    packet.status = (int)DataType.SYNACK;

                    Console.WriteLine("Server send SYNACK...");
                    newsock.Connect(sender);

                    string temp = packet.status + "-" + packet.packet;
                    send_buffer = Encoding.ASCII.GetBytes(temp);

                    newsock.Send(send_buffer, send_buffer.Length);
                }

                else if (packet.status == (int)DataType.ACK)
                {
                    Console.WriteLine("Server got ACK!");

                    if (counter >= 5000)
                        break;

                    tipe = DataType.ACK;
                    packet.status = (int)DataType.DATA;
                    packet.packet = counter;

                    Console.WriteLine("Server sending data ke : {0}", counter);
                    newsock.Connect(sender);

                    string temp = packet.status + "-" + packet.packet;
                    send_buffer = Encoding.ASCII.GetBytes(temp);

                    newsock.Send(send_buffer, send_buffer.Length);
                }

                else if (packet.status == (int)DataType.FIN)
                {
                    Console.WriteLine("Server got FIN!");

                    packet.status = (int)DataType.FINACK;
                    packet.packet = 0;

                    Console.WriteLine("Server send FINACK!");
                    newsock.Connect(sender);

                    string temp = packet.status + "-" + packet.packet;
                    send_buffer = Encoding.ASCII.GetBytes(temp);

                    newsock.Send(send_buffer, send_buffer.Length);
                }

                else if (packet.status == (int)DataType.FINACK)
                {

                }

                else if (packet.status == (int)DataType.DATA)
                {
                    Console.WriteLine("Server got DATA!");
                    counter += packet.packet;

                    if (counter % 50 == 0 && counter <= 5000)
                    {
                        packet.packet = counter;
                        packet.status = (int)DataType.DATA;

                        Console.WriteLine("Server send DATA...");
                        newsock.Connect(sender);

                        string temp = packet.status + "-" + packet.packet;
                        send_buffer = Encoding.ASCII.GetBytes(temp);

                        newsock.Send(send_buffer, send_buffer.Length);
                    }

                    else if (counter > 5000)
                    {
                        
                    }
                }
            }



            //data = newsock.Receive(ref sender);

            //Console.WriteLine("Message received from {0}:", sender.ToString());
            //Console.WriteLine(Encoding.ASCII.GetString(data, 0, data.Length));

            //string welcome = "Welcome to my test server";
            //data = Encoding.ASCII.GetBytes(welcome);
            //newsock.Send(data, data.Length, sender);

            //while (true)
            //{
            //    data = newsock.Receive(ref sender);

            //    Console.WriteLine(Encoding.ASCII.GetString(data, 0, data.Length));
            //    newsock.Send(data, data.Length, sender);
            //}

            Console.WriteLine("Closing server socket...");

        }
    }
}